import os
from exchangelib import DELEGATE, Account, Credentials
from datetime import datetime
import time

host = os.environ['imap_host']
username = os.environ['ad_username']
email = os.environ['imap_username']
password = os.environ['imap_password']
destination = os.environ['imap_destination']
autoreply = os.environ['nospam_uni_autoreply'] == "True"

credentials = Credentials(username=username, password=password)

my_account = Account(primary_smtp_address=email, credentials=credentials, autodiscover=True, access_type=DELEGATE)

whitelist = [k.lower().strip() for k in open("whitelist.csv", "r").readlines()]
answer = open("answer.txt", "r").read()
f_destination = my_account.inbox.parent / destination

print("---------- Running CRON ({}) ----------".format(datetime.now()))
for mail in my_account.inbox.all():
    sender = mail.sender.email_address.lower()
    direct = email in [k.email_address.lower() for k in mail.to_recipients] if mail.to_recipients else False
    in_cc = email in [k.email_address.lower() for k in mail.cc_recipients] if mail.cc_recipients else False
    unilist = "Message envoyé par Uniliste" in mail.text_body
    is_whitelisted = sender in whitelist

    print("Mail named {}".format(mail.subject))
    print("Directly adressed : {}".format(direct))

    print("In CC : {}".format(in_cc))
    print("Comes from UniList : {}".format(unilist))
    print("Is whitelisted : {}".format(is_whitelisted))

    if not is_whitelisted and not direct and not in_cc and unilist:
        mail.is_read = True
        mail.save()
        mail.move(f_destination)
        if autoreply:
            print("Selected action : move email to selected folder, mark as read and answer with email.")
            mail.reply(subject="RE (autoreply-uniliste) : {}".format(mail.subject), body=answer, to_recipients=[mail.sender.email_address])
        else:
            print("Selected action : move email to selected folder and mark as read.")

    print("------")
print("------------------------ Ending CRON ------------------------\n\n")
