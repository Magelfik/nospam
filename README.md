# Principe

Toutes les 5 minutes, le courrier qui n'est pas de la whitelist, qui ne vous est pas adressé directement sera considéré comme lu, aura une réponse via answer.txt et sera déplacé dans le dossier "Probablement spam".

# Installation

**Native**

- Lancez `pip install -r deployment/requirements.txt`
- Rendez-vous dans le dossier de l'app : ```cd app```
- Faites une copie du fichier ```start.example.sh``` en ```start.sh``` et adaptez les variables d'environnement
- Allez sur votre application de messagerie et créez un dossier nommé "Probablement Spam" (sans les guillemets)
- Lancez ```crontab -e```
- Ajoutez la ligne suivante : ```*/5 * * * * cd [DOSSIER APP]/app && bash start.sh >> [DOSSIER APP]/nospam.log 2>&1```

**Docker**

L'installation sur Docker permet de s'affranchir les installations de Python ainsi que des dépendances.

- Adaptez les variables d'environnement : ```cp app/start.example.sh app/start.sh; vim app/start.sh```

- Appeler le script toutes les 5 minutes :
  - exécuter ```crontab -e```
  - ajouter la ligne suivante : ```*/5 * * * * docker-compose -p nospam_uni -f [DOSSIER APP]/deployment/docker-compose.yml up```
  

Si nécessaire, les logs seront affichés ici : ```docker logs nospam_uni```.

  


